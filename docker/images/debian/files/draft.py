"""


    Xvfb:   /usr/X11/bin/xvfb
    xauth:  /opt/X11/bin/xauth


    on mac OS
        cd /usr/local/bin
        sudo ln -s /usr/X11/bin/xvfb Xvfb
        sudo ln -s /opt/X11/bin/xauth xauth



"""
import os
import time

from pyvirtualdisplay import Display
from selenium import webdriver

import logging
log= logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


os.environ['PATH'] += ":/usr/local/bin"


ardom_pattern= "samples/ardom-%s.html"


#DUMP="file://%s" % DUMP_FILENAME

class Env():
    """

    """
    #BROWSER= "firefox"
    BROWSER= "chrome"

display_backend= "xvfb"
#display_backend= "xvnc"


class Pager(object):
    """

        redefine WebKitPAge : add remote_control argument


    """
    def __init__(self,device='tv'):
        """

        :param remote_control: instance of RpiRemoteControl or equivalent
        :return:
        """
        self.device_alias= device
        #self.ardom_filename= "file:///ardom-%s.html" % self.device_alias
        self.ardom_filename = ardom_pattern % self.device_alias

        if Env.BROWSER == "firefox":
            self.display = Display(backend= display_backend, visible=0, size=(1024, 768))
            self.display.start()
            self.driver = webdriver.Firefox()
            self.driver.maximize_window()
        else:
            self.display = Display(backend=display_backend, visible=0, size=(1024, 768))
            self.display.start()
            self.driver = webdriver.Chrome()
            self.driver.maximize_window()

        self.logger = logging.getLogger('NewTvTesting.WebKitPage')
        #self.remote = RpiRemoteControl()


    def close(self):
        self.driver.quit()
        self.display.stop()







if __name__== "__main__":

    logging.basicConfig(level=logging.DEBUG)

    pager= Pager()

    print "wait 10s , then close"
    time.sleep(10)

    pager.close()

    print "Done"