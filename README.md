restop-selenium
===============



run the demo
------------

start the docker selenium grid
------------------------------

    cd docker/selenium-hub
    ./start.sh
    
it will start a hub and 3 firefox nodes


start the web demo server
-------------------------

    cd WebDemo/demoapp
    python server.py
    

run the test
------------

    cd tests
    python draft_remote.py


