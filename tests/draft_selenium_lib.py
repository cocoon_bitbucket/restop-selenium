from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import Selenium2Library



url= "http://192.168.1.23:7272"
browser= 'firefox'
remote_url= "http://192.168.99.100:4444/wd/hub"
desired_capabilities=DesiredCapabilities.FIREFOX

username= 'demo'
password= 'mode'


session= Selenium2Library.Selenium2Library(timeout=3)


session.open_browser(url,
                     browser=browser,
                     alias="me",
                     remote_url=remote_url,
                     desired_capabilities=desired_capabilities,
                     ff_profile_dir=None)

try:


    session.maximize_browser_window()
    session.set_selenium_speed(0)

    session.title_should_be('Login Page')
    session.capture_page_screenshot(filename='screenshot.png')

    session.input_text('username_field' , username)
    session.input_password('password_field',password)

    session.click_button('login_button')

    #session.location_should_be("")
    session.title_should_be('Welcome Page')
    #Location Should Be    ${WELCOME URL}
    #Title Should Be    Welcome Page
    session.capture_page_screenshot(filename='logged_in.png')

except Exception, e:
    print "error: %s" % str(e)

finally:
    session.close_browser()


print "Done"