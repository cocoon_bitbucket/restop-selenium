from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

"""

docker run -d -p "4444:4444" selenium/standalone-firefox



selenium2Library

   def open_browser(self, url, browser='firefox', alias=None,remote_url=False,
                desired_capabilities=None,ff_profile_dir=None):


Optional 'remote_url' is the url for a remote selenium server for example
        http://127.0.0.1:4444/wd/hub.
        If you specify a value for remote you can
        also specify 'desired_capabilities' which is a string in the form
        key1:val1,key2:val2 that will be used to specify desired_capabilities
        to the remote server.

        This is useful for doing things like specify a
        proxy server for internet explorer or for specify browser and os if your
        using saucelabs.com. 'desired_capabilities' can also be a dictonary
        (created with 'Create Dictionary') to allow for more complex configurations.
        Optional 'ff_profile_dir' is the path to the firefox profile dir if you
        wish to overwrite the default.


    desired_capabilities=
        {'platform': 'ANY', 'browserName': 'firefox', 'version': '', 'marionette': True, 'javascriptEnabled': True}


"""
host= '192.168.99.100'

driver = webdriver.Remote(
   command_executor='http://%s:4444/wd/hub' % host,
   desired_capabilities=DesiredCapabilities.FIREFOX)

# driver2 = webdriver.Remote(
#    command_executor='http://%s:4444/wd/hub' % host,
#    desired_capabilities=DesiredCapabilities.FIREFOX)
# driver2.close()


driver.get('http://192.168.1.23:7272')
#driver.get('http://192.168.1.152:7272')


driver.maximize_window()

pic= driver.get_screenshot_as_png()
with open("pic.png" ,"wb") as fh:
    fh.write(pic)

#driver.quit()
driver.close()

# driver = webdriver.Remote(
#    command_executor='http://%s:4444/wd/hub' % host,
#    desired_capabilities=DesiredCapabilities.FIREFOX)
#
#
#
#
#
# driver.get('http://192.168.1.23:7272/toto')
#
# driver.maximize_window()
#
# pic= driver.get_screenshot_as_png()
# with open("pic.png" ,"wb") as fh:
#     fh.write(pic)
#
# driver.quit()

# driver = webdriver.Remote(
#    command_executor='http://127.0.0.1:4444/wd/hub',
#    desired_capabilities=DesiredCapabilities.OPERA)
#
# driver = webdriver.Remote(
#    command_executor='http://127.0.0.1:4444/wd/hub',
#    desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)
